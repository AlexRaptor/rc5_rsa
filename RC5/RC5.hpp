//
//  RC5.hpp
//  RC5
//
//  Created by Александр Селиванов on 09.12.2017.
//  Copyright © 2017 Alexander Selivanov. All rights reserved.
//

#ifndef RC5_hpp
#define RC5_hpp

#include <cstdlib>
#include <stdlib.h>
#include <string>
#include <iostream>

using namespace std;

#define max(a, b) \
({ __typeof__ (a) _a = (a); \
__typeof__ (b) _b = (b); \
_a > _b ? _a : _b; })

typedef unsigned char uchar;
typedef unsigned long long ulong;
typedef unsigned int uint;

class RC5 {
    
private:
    const uint ROUNDS = 15;
    const uint WORD_LENGTH = 64;
    const ulong Pw = 0xB7E151628AED2A6B;
    const ulong Qw = 0x9E3779B97F4A7C15;
    const uchar SECRET_SIGN = '#';
    
    uchar *p_key;
    ulong *p_K;
    
public:
    RC5(string keyString) {
        uint keyBytes = buildKey(keyString);
        
        int i, iu, u = WORD_LENGTH / 8, m;
        int j, c = keyBytes / u, s, t;
        union {
            uchar byte[4];
            ulong word;
        } byte_word;
        ulong a, b, *p_L;
        
        p_L = (ulong *) calloc(c, sizeof(ulong));
        
        for (i = keyBytes; i <= c * u - 1; i++) {
            p_key[i] = 0;
        }
        
        for (i = 0; i < c; i++) {
            iu = i * u;
            for (j = 0; j < u; j++) {
                byte_word.byte[j] = p_key[iu + j];
            }
            p_L[i] = byte_word.word;
        }
        p_K = new ulong[2 * ROUNDS + 2];
        
        p_K[0] = Pw;
        for (i = 1; i <= 2 * ROUNDS + 1; i++) {
            p_K[i] = p_K[i - 1] + Qw;
        }
        
        i = j = 0;
        a = b = 0;
        m = 2 * ROUNDS + 2;
        t = max(c, m);
        for (s = 1; s <= 3 * t; s++) {
            p_K[i] = RC5_rotate_left(p_K[i] + a + b, 3);
            a = p_K[i];
            i = (i + 1) % m;
            p_L[j] = RC5_rotate_left(p_L[j] + a + b, (a + b) & (WORD_LENGTH - 1));
            b = p_L[j];
            j = (j + 1) % c;
        }
        free(p_L);
    }
    
    virtual ~RC5() {
        delete[] p_key;
        delete[] p_K;
    }
    
private:
    ulong RC5_rotate_left(ulong val, ulong dist) {
        return (val << dist) | (val >> (WORD_LENGTH - dist));
    }
    
    ulong RC5_rotate_right(ulong val, ulong dist) {
        return (val >> dist) | (val << (WORD_LENGTH - dist));
    }
    
    void encrypt(ulong inA, ulong inB, ulong *outA, ulong *outB) {
        int i, i2;
        
        inA += p_K[0];
        inB += p_K[1];
        
        for (i = 1; i <= ROUNDS; ++i) {
            i2 = i * 2;
            inA = RC5_rotate_left(inA ^ inB, inB & (WORD_LENGTH - 1)) + p_K[i2];
            inB = RC5_rotate_left(inB ^ inA, inA & (WORD_LENGTH - 1)) + p_K[i2 + 1];
        }
        
        *outA = inA;
        *outB = inB;
    }
    
    void decrypt(ulong inA, ulong inB, ulong *outA, ulong *outB) {
        int i, i2;
        
        for (i = ROUNDS; i >= 1; i--) {
            i2 = i * 2;
            inB = RC5_rotate_right(inB - p_K[i2 + 1], inA & (WORD_LENGTH - 1)) ^ inA;
            inA = RC5_rotate_right(inA - p_K[i2], inB & (WORD_LENGTH - 1)) ^ inB;
        }
        
        *outA = inA - p_K[0];
        *outB = inB - p_K[1];
    }
    
#pragma mark - initialization
    
    string resizeString(string str, int size, char ch) {
        string res = str;
        while (res.length() < size) {
            res = ch + res;
        }
        return res;
    }
    
    string resizeKey(string keyString) {
        if ((keyString.length() % (WORD_LENGTH / 8)) != 0) {
            uint newStringSize = ( keyString.length() / (WORD_LENGTH / 8) + 1 ) * (WORD_LENGTH / 8);
            keyString = resizeString(keyString, newStringSize, SECRET_SIGN);
        }
        return keyString;
    }
    
    uint buildKey(string keyString) {
        keyString = resizeKey(keyString);
        p_key = new uchar[keyString.length()];
        
        for (int i = 0; i < keyString.length(); ++i) {
            p_key[i] = keyString[i];
        }
        
        return keyString.length();
    }
    
#pragma mark - encoding
    
    void string2ulong(string str, ulong *arr) {
        for (int i = 0; i < str.length(); ++i) {
            arr[i] = static_cast<ulong>(str[i]);
        }
    }
    
    string ulong2ucharString(ulong val) {
        string res = "";
        
        for (int i = 0; i < 8; ++i) {
            
            uchar seg = val - ((val >> 8) << 8);
            val = (val - seg) >> 8;
            uchar a = 0 + seg / 222 + 33;
            uchar b = 0 + seg % 222 + 33;
            res += a;
            res += b;
        }
        
        return res;
    }
    
    string ulongArr2string(ulong *arr, uint arraySize) {
        string res = "";
        for (int i = 0; i < arraySize; ++i) {
            string ucharString = ulong2ucharString(arr[i]);
            res += ucharString;
        }
        return res;
    }
    
#pragma mark - decoding
    
    string ulong2string(ulong *arr, int size) {
        string str = "";
        for (int i = 0; i < size; ++i) {
            str += (uchar) arr[i];
        }
        return str;
    }
    
    ulong ucharString2ulong(string str) {
        ulong res = 0;
        for (int i = str.length() - 2; i >= 0; i -= 2) {
            res <<= 8;
            res += (ulong)((uchar)str[i] - 33) * 222 + (uchar)str[i + 1] - 33;
        }
        return res;
    }
    
    void string2ulongArr2(string str, int arrSize, ulong *arr) {
        for (int i = 0; i < arrSize; ++i) {
            arr[i] = ucharString2ulong(str.substr(i * 16, 16));
        }
    }
    
public:
    
    string encode(string inputString) {

        int stringSize = inputString.length();

        ulong *encArr = new ulong[inputString.length()];
        string2ulong(inputString, encArr);
        
        ulong a, b;
        
        for (int i = 0; i < stringSize - 1; ++i) {
            encrypt(encArr[i], encArr[i + 1], &a, &b);
            encArr[i] = a;
            encArr[i + 1] = b;
        }
        
        string encString = ulongArr2string(encArr, stringSize);
        
        return encString;
        
    }
    
    string decode(string inputString) {
        
        int stringSize = inputString.length() / 2 / 8;
        
        ulong decArr[stringSize];
        string2ulongArr2(inputString, stringSize, decArr);
        
        ulong a, b;
        
        for (int i = stringSize - 1; i > 0; --i) {
            decrypt(decArr[i - 1], decArr[i], &a, &b);
            decArr[i - 1] = a;
            decArr[i] = b;
        }
        
        string decString = ulong2string(decArr, stringSize);
        return decString;
    }
};

#endif /* RC5_hpp */
