//
//  RSA.m
//  RC5
//
//  Created by Александр Селиванов on 16.01.2018.
//  Copyright © 2018 Alexander Selivanov. All rights reserved.
//

#import "RSA.h"
#include "gmp.h"

@implementation RSA

#pragma mark - utilities

+ (NSDecimalNumber *)remainderOnDividing:(NSDecimalNumber *)dividend by:(NSDecimalNumber *)divisor {
    
    NSDecimalNumber *quotient = [dividend decimalNumberByDividingBy:divisor
                                                       withBehavior:[NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundDown scale:0 raiseOnExactness:NO raiseOnOverflow:NO raiseOnUnderflow:NO raiseOnDivideByZero:NO]];
    
    NSDecimalNumber *subtractAmount = [quotient decimalNumberByMultiplyingBy:divisor];
    
    NSDecimalNumber *remainder = [dividend decimalNumberBySubtracting:subtractAmount];
    
    return remainder;
    
}

#pragma mark - key generation

+ (BOOL)isPrime:(ulong)value {
    
    if (value < 2) {
        return false;
    }
    
    if (value == 2) {
        return true;
    }
    
    for (ulong i = 3; i < value; ++i) {
        if (value % i == 0) {
            return false;
        }
    }
    
    return YES;
}

+ (ulong)calculateFermatNumber:(ulong)n {
    
    return powl(2, powl(2, n)) + 1;
    
}

+ (ulong)calculateEwithM:(ulong)m {
    
    ulong n = arc4random() % 5;
    ulong e = [self calculateFermatNumber:n++];
    
    for (ulong i = 2; i <= e; ++i) {

        if ((e % i == 0) && (m % i == 0)) {
            e = [self calculateFermatNumber:n++];
            i = 1;
        }
    }

    if (e >= m) {
    
        e = m - 1;
    
        for (ulong i = 2; i <= e; ++i) {
            if ((e % i == 0) && (m % i == 0)) {
                e--;
                i = 1;
            }
        }
    }
    
    return e;
}

+ (ulong)calculateDwithE:(ulong)e andM:(ulong)m {
    
    ulong d = 1;
    
    do {
        if (d != e && ((d * e) % m) == 1) {
            return d;
        } else {
            d++;
        }
    } while (true);
}

+ (void)generateKeysFromP:(ulong)p andQ:(ulong)q toPubE:(ulong *)e toPrivD:(ulong *)d toModuleN:(ulong *)n {
    
    *n = p * q;
    ulong m = (p - 1) * (q - 1);
    *e = [self calculateEwithM:m];
    *d = [self calculateDwithE:*e andM:m];

}

#pragma mark - encoding

+ (NSString *)rsaEncodeCharacter:(mpz_t)ch withE:(mpz_t)e andN:(mpz_t)n {
    
    mpz_t enc;
    mpz_init(enc);
    
    mpz_powm(enc, ch, e, n);
    char *out_str = mpz_get_str(NULL, 10, enc);
    
    NSString *res = [NSString stringWithUTF8String:out_str];
    
    mpz_clear(enc);
    
    return res;
  
}

+ (NSString *)encodeText:(NSString *)string withE:(NSDecimalNumber *)e andN:(NSDecimalNumber *)n {
    
    NSString *res = @"";

    mpz_t e_m, n_m;
    
    mpz_init(e_m);
    mpz_init(n_m);
    
    mpz_set_ui(e_m, [e unsignedIntValue]);
    mpz_set_ui(n_m, [n unsignedIntValue]);
    
    for (NSUInteger i = 0; i < [string length]; ++i)
    {

        mpz_t c_m;
        mpz_init(c_m);
        
        mpz_set_ui(c_m, [string characterAtIndex:i]);
        
        NSString *encodedCharacter = [self rsaEncodeCharacter:c_m withE:e_m andN:n_m];
        
        mpz_clear(c_m);
        
        res = [res stringByAppendingFormat: @"%@ ", encodedCharacter];
        
    }
    
    mpz_clear(n_m);
    mpz_clear(e_m);
    
    return [res stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
}

#pragma mark - decoding


+ (NSString *)rsaDecodeCharacter:(mpz_t)ch withD:(mpz_t)d andN:(mpz_t)n {
    
    mpz_t dec;
    mpz_init(dec);
    
    mpz_powm(dec, ch, d, n);
    unichar out_str = (unichar)mpz_get_ui(dec);
    
    NSString *res = [NSString stringWithCharacters:&out_str length:1];
    
    mpz_clear(dec);
    
    return res;
    
}

+ (NSString *)decodeText:(NSString *)string withD:(NSDecimalNumber *)d andN:(NSDecimalNumber *)n {
    
    NSString *res = @"";
    mpz_t d_m;
    mpz_t n_m;
    mpz_init(d_m);
    mpz_init(n_m);
    
    mpz_set_ui(d_m, [d unsignedIntValue]);
    mpz_set_ui(n_m, [n unsignedIntValue]);
    
    NSArray *arrayOfChars = [string componentsSeparatedByString:@" "];
    
    for (NSString *obj in arrayOfChars) {
        
        mpz_t c_m;
        mpz_init(c_m);
        mpz_set_ui(c_m, [obj integerValue]);
        
        NSString *str = [self rsaDecodeCharacter:c_m withD:d_m andN:n_m];
        
        mpz_clear(c_m);
        
        res = [res stringByAppendingString:str];
        
    }
    
    mpz_clear(n_m);
    mpz_clear(d_m);
    
    return res;
}

@end
