//
//  AppDelegate.m
//  RC5
//
//  Created by Александр Селиванов on 09.12.2017.
//  Copyright © 2017 Alexander Selivanov. All rights reserved.
//

#import "AppDelegate.hh"
#include "RC5.hpp"
#import "RSA.h"
#import "gmp.h"

#define MIN_RND 1000
#define MAX_RND 100000

@interface AppDelegate ()

@property (weak) IBOutlet NSWindow *window;
@property (weak) IBOutlet NSSecureTextField *rc5KeyView;

@property (weak) IBOutlet NSTextView *rc5TextView;
@property (weak) IBOutlet NSTextField *rc5ResultLabel;
@property (weak) IBOutlet NSSecureTextField *rsaPublicKeyView;
@property (weak) IBOutlet NSSecureTextField *rsaPrivateKeyView;
@property (weak) IBOutlet NSTextView *rsaTextView;
@property (weak) IBOutlet NSTextField *rsaResultLabel;

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
    [[self window] setTitle:@"RC5"];
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

- (IBAction)rc5EncodeButton:(id)sender {
        
    NSString *key = [[self rc5KeyView] stringValue];
    RC5 rc5 = RC5([key cStringUsingEncoding: [NSString defaultCStringEncoding]]);

    NSString *inputText = [[self rc5TextView] string];
    
    string inputString = [inputText cStringUsingEncoding:[NSString defaultCStringEncoding]];
    
    NSDate *methodStart = [NSDate date];
    
    string encodedString = rc5.encode(inputString);
    
    NSDate *methodFinish = [NSDate date];
    NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:methodStart];
    
    NSString *es = [NSString stringWithCString:encodedString.c_str()
                                      encoding:[NSString defaultCStringEncoding]];
    
    [[self rc5ResultLabel] setStringValue:[NSString stringWithFormat:
                                           @"length: %lu / %lu, executionTime: %f",
                                           (unsigned long)[inputText length],
                                           (unsigned long)[es length], executionTime]];
    
    [[self rc5TextView] setString: es];
    
}

- (IBAction)rc5DecodeButton:(id)sender {
    
    NSString *key = [[self rc5KeyView] stringValue];
    RC5 rc5 = RC5([key cStringUsingEncoding: [NSString defaultCStringEncoding]]);
    
    NSString *inputText = [[self rc5TextView] string];
    
    string inputString = [inputText cStringUsingEncoding:[NSString defaultCStringEncoding]];
    
    NSDate *methodStart = [NSDate date];
    
    string decodedString = rc5.decode(inputString);
    
    NSDate *methodFinish = [NSDate date];
    NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:methodStart];
    
    NSString *ds = [NSString stringWithCString:decodedString.c_str()
                                      encoding:[NSString defaultCStringEncoding]];
    
    [[self rc5ResultLabel] setStringValue:[NSString stringWithFormat:
                                           @"length: %lu / %lu, executionTime: %f",
                                           (unsigned long)[inputText length],
                                           (unsigned long)[ds length], executionTime]];
    
    [[self rc5TextView] setString: ds];
    
}

- (IBAction)rsaGenerateKeys:(id)sender {
    
    NSDate *methodStart = [NSDate date];
    
    ulong p;
    do {
        p = arc4random() % MAX_RND + MIN_RND;
    } while (![RSA isPrime:p]);
    
    ulong q;
    do {
        q = arc4random() % MAX_RND + MIN_RND;
    } while (![RSA isPrime:q]);
    
    ulong e, d, n;
    
    [RSA generateKeysFromP:p andQ:q toPubE:&e toPrivD:&d toModuleN:&n];
    
    NSDate *methodFinish = [NSDate date];
    NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:methodStart];
    
    [[self rsaResultLabel] setStringValue:[NSString stringWithFormat:
                                           @"from %d to %d, executionTime: %f",
                                           MIN_RND, MAX_RND, executionTime]];

    [[self rsaTextView] setString:[NSString stringWithFormat:
                                   @"public key:\n%llu,%llu\n\nprivate key:\n%llu,%llu",
                                   e, n, d, n]];
}

- (IBAction)rsaEncodeButton:(id)sender {

    NSString *publicKey = [[[self rsaPublicKeyView] stringValue] stringByReplacingOccurrencesOfString:@" " withString:@""];

    NSDecimalNumber *e = [NSDecimalNumber decimalNumberWithString:[[publicKey componentsSeparatedByString:@","] objectAtIndex:0]];
    NSDecimalNumber *n = [NSDecimalNumber decimalNumberWithString:[[publicKey componentsSeparatedByString:@","] objectAtIndex:1]];

    NSString *inputText = [[self rsaTextView] string];

    NSDate *methodStart = [NSDate date];

    NSString *encodedString = [RSA encodeText:inputText withE:e andN:n];

    NSDate *methodFinish = [NSDate date];
    NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:methodStart];

    [[self rsaResultLabel] setStringValue:[NSString stringWithFormat:@"length: %lu / %lu, executionTime: %f", (unsigned long)[inputText length], (unsigned long)[encodedString length], executionTime]];

    [[self rsaTextView] setString:encodedString];

}

- (IBAction)rsaDecodeButton:(id)sender {
    
    NSString *privateKey = [[[self rsaPrivateKeyView] stringValue] stringByReplacingOccurrencesOfString:@" " withString:@""];

    NSDecimalNumber *d = [NSDecimalNumber decimalNumberWithString:[[privateKey componentsSeparatedByString:@","] objectAtIndex:0]];
    NSDecimalNumber *n = [NSDecimalNumber decimalNumberWithString:[[privateKey componentsSeparatedByString:@","] objectAtIndex:1]];
    
    NSString *inputText = [[self rsaTextView] string];
    
    NSDate *methodStart = [NSDate date];
    
    NSString *decodedString = [RSA decodeText:inputText withD:d andN:n];
    
    NSDate *methodFinish = [NSDate date];
    NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:methodStart];
    
    [[self rsaResultLabel] setStringValue:[NSString stringWithFormat:@"length: %lu / %lu, executionTime: %f", (unsigned long)[inputText length], (unsigned long)[decodedString length], executionTime]];
    
    [[self rsaTextView] setString:decodedString];
    
}

@end
