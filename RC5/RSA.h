//
//  RSA.h
//  RC5
//
//  Created by Александр Селиванов on 16.01.2018.
//  Copyright © 2018 Alexander Selivanov. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef unsigned long long ulong;

@interface RSA : NSObject

+ (BOOL)isPrime:(ulong)value;
+ (void)generateKeysFromP:(ulong)p andQ:(ulong)q toPubE:(ulong *)e toPrivD:(ulong *)d toModuleN:(ulong *)n;
+ (NSString *)encodeText:(NSString *)string withE:(NSDecimalNumber *)e andN:(NSDecimalNumber *)n;
+ (NSString *)decodeText:(NSString *)string withD:(NSDecimalNumber *)d andN:(NSDecimalNumber *)n;

@end
