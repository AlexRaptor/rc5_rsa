//
//  AppDelegate.h
//  RC5
//
//  Created by Александр Селиванов on 09.12.2017.
//  Copyright © 2017 Alexander Selivanov. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

