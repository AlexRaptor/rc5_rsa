//
//  main.m
//  RC5
//
//  Created by Александр Селиванов on 09.12.2017.
//  Copyright © 2017 Alexander Selivanov. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
